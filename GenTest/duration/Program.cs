﻿using System;
using System.IO;

namespace duration
{
    class MainClass
    {
        [STAThread]
        static int Main(string[] args)
        {
            // Test if input arguments were supplied.
            if (args.Length == 0)
            {
                Console.WriteLine("Please enter the time from and the time to.");
                return 1;
            }
            string sTimeFrom = args[0];
            string sTimeTo = args[1];
            if (String.IsNullOrEmpty(sTimeFrom))
            {
                Console.WriteLine("Time from is empty.");
                return 1;
            }            
            if (String.IsNullOrEmpty(sTimeTo))
            {
                Console.WriteLine("Time to is empty.");
                return 1;
            }            

            TimeSpan tsFrom = TimeSpan.Parse(sTimeFrom);    
            TimeSpan tsTo = TimeSpan.Parse(sTimeTo);    
            string sDuration = $"{tsTo.Duration() - tsFrom.Duration()}";  

            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "durationresults.txt"), true))
            {
                outputFile.WriteLine($"{sDuration} <- (From {tsFrom} -> To: {tsTo})");
            }            

            Console.WriteLine($"Duration = {sDuration}");
            
            return 0;
        }
    }
}
