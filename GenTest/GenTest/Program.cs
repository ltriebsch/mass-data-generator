﻿using System;
using System.Collections.Generic;
using System.IO;

namespace genTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> lines = new List<string>();
            lines.Add("/* MMASCII v1.4 seq=true */;;;;;;;;;;;;;;;;;;;;;;;");

            string currentHolder = "H-Test-3"; 
            lines.Add($"[InhaberEx: {currentHolder};Modify+;;;{currentHolder};01.01.2012;TestData;];;;;;;;;;;;;;;;;");
            
            string currentPortfolio = "P-Test-3"; 
            lines.Add($"[Portfolio: {currentPortfolio};Modify+;{currentHolder};{currentPortfolio};];;;;;;;;;;;;;;;;;;;");

            string currentAccount = "K-Test-3"; 
            string currentAccountBIC = "DETest00000000003"; 
            lines.Add($"[Konto: {currentAccount};Modify+;{currentHolder};Nummer;{currentAccount};;;EUR; ;Performance;;;;;;;;{currentPortfolio};{currentAccountBIC};TEST12345;];;;");

            string currentSecAccount = "D-Test-3"; 
            lines.Add($"[Depot: {currentSecAccount};Modify+;{currentAccount};Nummer;{currentSecAccount};;;;;;;;;;;;;;;;{currentPortfolio};;TEST12345;]");

            string entryId = "tres";

            string currentBuchung; 
            for (int i = 0; i < 1000; i++)
            {
                currentBuchung = $"[Buchung: {currentAccount};Bareinlage;100;01.01.2012;30.12.2011;;EUR;1;;;Meine Bemerkung ;{entryId}{i};;EUR;];;;;;;;;;";   
                lines.Add(currentBuchung);
            }

            string currentOrder; 
            for (int i = 0; i < 1000; i++)
            {
                currentOrder = $"[Order: {currentSecAccount};DE0007100000;11.03.2016;Einbuchung;0;500;4;;27.08.2009;11:11;EUR;1;;;;;Testorder Ankauf;0;0;0;{entryId}l{i};];;";   
                lines.Add(currentOrder);
            }

            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, $"Test_{entryId}.csv")))
            {
                foreach (string line in lines)
                    outputFile.WriteLine(line);
            }
        }
    }
}
