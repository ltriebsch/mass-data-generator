﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace genTestUI
{
    public class GenTest
    {
        public string TestName { get; }
        public string ObjectNames { get; }

        public int Holders { get; }
        public int Entries { get; }
        public int Orders { get; }
        
        private List<string> lines = new List<string>();

        public GenTest(string testName, string objectNames, int holders, int entries, int orders)
        {
            this.TestName = testName;
            this.ObjectNames = objectNames;
            this.Holders = holders;
            this.Entries = entries;
            this.Orders = orders;
        }

        private string generateBIC(int index)
        {
            TimeSpan timeStamp = new TimeSpan(DateTime.Now.Ticks);
            Int32 unixTimestamp = (Int32)timeStamp.TotalSeconds;

            string sValue = $"DE{unixTimestamp}";  
            sValue = sValue.Replace("-", string.Empty);
            string sIndex = $"{index}";
            while (sValue.Length + sIndex.Length < 17)
            {
                sIndex = $"0{sIndex}";    
            }
            return sValue + sIndex; 
        }
        private void makeHolder(int index)
        {
            string currentHolder = $"H-{ObjectNames}_{index}"; 
            lines.Add($"[InhaberEx: {currentHolder};Modify+;;;{currentHolder};01.01.2012;TestData;];;;;;;;;;;;;;;;;");
            
            string currentPortfolio = $"P-{ObjectNames}_{index}"; 
            lines.Add($"[Portfolio: {currentPortfolio};Modify+;{currentHolder};{currentPortfolio};];;;;;;;;;;;;;;;;;;;");

            string currentAccount = $"K-{ObjectNames}_{index}"; 
            string currentAccountBIC = generateBIC(index);
            lines.Add($"[Konto: {currentAccount};Modify+;{currentHolder};Nummer;{currentAccount};;;EUR; ;Performance;;;;;;;;{currentPortfolio};{currentAccountBIC};TEST12345;];;;");

            string currentSecAccount = $"D-{ObjectNames}_{index}"; 
            lines.Add($"[Depot: {currentSecAccount};Modify+;{currentAccount};Nummer;{currentSecAccount};;;;;;;;;;;;;;;;{currentPortfolio};;TEST12345;]");

            string currentBuchung; 
            for (int i = 0; i < Entries; i++)
            {
                currentBuchung = $"[Buchung: {currentAccount};Bareinlage;100;01.01.2012;30.12.2011;;EUR;1;;;Meine Bemerkung ;{TestName}-{index}-{i};;EUR;];;;;;;;;;";   
                lines.Add(currentBuchung);
            }

            string currentOrder; 
            for (int i = 0; i < Orders; i++)
            {
                currentOrder = $"[Order: {currentSecAccount};DE0007100000;11.03.2016;Einbuchung;0;500;4;;27.08.2009;11:11;EUR;1;;;;;Testorder Ankauf;0;0;0;{TestName}l-{index}-{i};];;";   
                lines.Add(currentOrder);
            }

        }    
        public void makeCVSFile()
        {
            lines.Clear();
            lines.Add("/* MMASCII v1.4 seq=true */;;;;;;;;;;;;;;;;;;;;;;;");

            for (int i = 0; i < Holders; i++)
            {
                makeHolder(i);
            }

            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, $"Test_{TestName}.csv")))
            {
                foreach (string line in lines)
                    outputFile.WriteLine(line);
            }

        }
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            string sTestname = testName.Text;
            string sObjecNames = objectNames.Text;
            string sEntries = entries.Text;
            string sOrders = orders.Text;
            string sHolders = holders.Text;
            GenTest genTest = new GenTest(sTestname, sObjecNames, GetInputAsInteger(sHolders), GetInputAsInteger(sEntries), GetInputAsInteger(sOrders));
            genTest.makeCVSFile();
            Close();
        }

        private int GetInputAsInteger(string sValue)
        {
            int nReturnValue = 0;
            if( !String.IsNullOrEmpty(sValue) )
            {
                if(int.TryParse(sValue, out nReturnValue))
                {
                    return nReturnValue;
                }
            }
            return nReturnValue;
        }
    }
}
