﻿using System;
using System.IO;

namespace searchUploadLog
{
    class Program
    {
        private static bool ReadUploadTimeFromLine(string sLine, string logMark, out TimeSpan logTimeSpan)
        {
            bool rtnValue = sLine.Contains(logMark);
            logTimeSpan = new TimeSpan();
            if(!rtnValue)
            {
                return rtnValue;
            }

            string sTime = sLine.Substring(11, 13);
            logTimeSpan = TimeSpan.Parse(sTime);

            return rtnValue;
        }    
        private static bool ReadUploadTime(string fileName, string logMark, bool minValue, out TimeSpan logTimeSpan)
        {
            bool rtnValue = false;
            if(minValue)
            {
                logTimeSpan = new TimeSpan(1,59,59,59,999);
            }
            else
            {
                logTimeSpan = new TimeSpan(0,0,0,0,001);
            }    
            if(!File.Exists(fileName))
            {
                return rtnValue;
            }

            TimeSpan currentTimeSpan;
            string[] lines = System.IO.File.ReadAllLines(fileName);
            foreach (string line in lines)
            {
                if(ReadUploadTimeFromLine(line, logMark, out currentTimeSpan))
                {
                    if(minValue)
                    {
                        if(currentTimeSpan < logTimeSpan)
                        {
                            logTimeSpan = currentTimeSpan; 
                            rtnValue = true;   
                        }
                    }
                    else
                    {
                        if(currentTimeSpan > logTimeSpan)
                        {
                            logTimeSpan = currentTimeSpan; 
                            rtnValue = true;   
                        }
                    }    
                } 
            }
            return rtnValue;
        }
        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please enter the log path.");
                return 1;
            }

            string sLogPath = args[0];
            if(!Directory.Exists(sLogPath)) 
            {
                Console.WriteLine("{0} is not a valid file or directory.", sLogPath);
                return 1;
            }

            TimeSpan currentTimeSpan;
            TimeSpan timeSpanUpload = new TimeSpan(1,59,59,59,999);
            TimeSpan timeSpanUploadDone = new TimeSpan(0,0,0,0,001);
            DirectoryInfo dirInfo = new DirectoryInfo(sLogPath);
            FileInfo[] Files = dirInfo.GetFiles("vwdpm*.log"); //Getting log files
            foreach(FileInfo file in Files )
            {
                if(ReadUploadTime($"{file.DirectoryName}\\{file.Name}", "UploadData", true, out currentTimeSpan))
                {
                    if(currentTimeSpan < timeSpanUpload)
                    {
                        timeSpanUpload = currentTimeSpan;   
                    }
                }
                if(ReadUploadTime($"{file.DirectoryName}\\{file.Name}", "UploadData done", false, out currentTimeSpan))
                {
                    if(currentTimeSpan > timeSpanUploadDone)
                    {
                        timeSpanUploadDone = currentTimeSpan;   
                    }
                }
            }

            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "UploadDataLog.txt"), true))
            {
                outputFile.WriteLine($"Upload begin: {timeSpanUpload}");
                outputFile.WriteLine($"Upload done begin: {timeSpanUploadDone}");
            }            

            Console.WriteLine($"Upload begin: {timeSpanUpload}");
            Console.WriteLine($"Upload begin: {timeSpanUploadDone}");

            return 0;
        }
    }
}
